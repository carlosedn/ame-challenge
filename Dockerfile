FROM maven:3.5.2-jdk-8 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn  -f /usr/src/app/pom.xml clean package


FROM openjdk:8-jdk-alpine
COPY --from=build /usr/src/app/target/ame-challenge-0.0.1-SNAPSHOT.jar /usr/app/ame-challenge-0.0.1-SNAPSHOT.jar
EXPOSE 9598
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=prod","-jar","/usr/app/ame-challenge-0.0.1-SNAPSHOT.jar"]
