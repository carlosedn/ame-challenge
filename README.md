# Challenge Back-End AME
This repo contains the project Challenge Back-End AME.

## Getting started

### Run local

##### build the application local
*       command: mvn clean package
##### start the application local
*		command: java -jar target/ame-challenge-0.0.1-SNAPSHOT.jar


### Run in docker

* Then in your terminal run: `docker-compose up --build`.
* Will be created a folder dist.
* To deploy, upload the folder dist.

### Endpoint to access API

* List planets from StarWars API - GET
*       http://localhost:9598/api/planetsFromStarWars?page=1
* Create planet - POST
*        http://localhost:9598/api/planets/save
        {
        	"name":"Utapau",
        	"climate": "temperate, arid, windy",
        	"terrain": "scrublands, savanna, canyons, sinkholes"
        }
* List planets from database - GET
*       http://localhost:9598/api/planets/
* Find planets by name - GET
*       http://localhost:9598/api/planets/search/nameStartsWith?name=Utapau
* Find planets by ID - GET
*       http://localhost:9598/api/planets/{id}
* Delete planet by ID - DELETE
*       http://localhost:9598/api/planets/{id}



