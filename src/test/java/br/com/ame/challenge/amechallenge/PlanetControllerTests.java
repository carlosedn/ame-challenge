package br.com.ame.challenge.amechallenge;

import br.com.ame.challenge.amechallenge.model.Planet;
import br.com.ame.challenge.amechallenge.repository.PlanetRepository;
import br.com.ame.challenge.amechallenge.service.PlanetService;
import br.com.ame.challenge.amechallenge.service.StarWarsService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlanetControllerTests {

    @Autowired
    WebTestClient testClient;

    @MockBean
    private StarWarsService starWarsServiceMock;
    @MockBean
    private PlanetService planetServiceMock;
    @Mock
    private PlanetRepository planetRepository;


    @Test
    public void testGetPlanetEmptyReturnNotFound(){
        Optional<Planet> planet = Optional.empty();
        given(planetRepository.findById(anyLong())).willReturn(planet);
        testClient.get().uri("/api/planets/99").exchange().expectStatus().isNotFound();
    }


    @Test
    public void testGetPlanetsEmptyReturnOK(){
        Page<Planet> planet = Page.empty();
        given(planetRepository.findByNameStartsWith(anyString(), Mockito.any(PageRequest.class))).willReturn(planet);
        testClient.get().uri("/api/planets/").exchange().expectStatus().isOk();
    }

    @Test
    public void testPostPlanets(){

        Planet planet = Planet.builder().Id(1L).name("test1").climate("unknow").terrain("unknow").qtFilms(0).build();
        testClient.post().uri("/api/planets/save")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(planet), Planet.class)
                .exchange()
                .expectStatus().isOk();
    }


    @Test
    public void testDeletePlanetsReturnNotFound(){

        testClient.delete().uri("/api/planets/99")
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void testGetPlanetFromStarWars(){
        testClient.get().uri("/api/planetsFromStarWars")
                .exchange()
                .expectStatus().isOk();
    }


}
