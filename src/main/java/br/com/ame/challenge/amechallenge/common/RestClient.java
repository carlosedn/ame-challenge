package br.com.ame.challenge.amechallenge.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

public class RestClient {

    protected ResponseEntity<String> getRest(String url) throws URISyntaxException, JsonProcessingException, HttpClientErrorException {
        URI uri = new URI(url);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36");

        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        return restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

    }

}
