package br.com.ame.challenge.amechallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmeChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmeChallengeApplication.class, args);
	}

}
