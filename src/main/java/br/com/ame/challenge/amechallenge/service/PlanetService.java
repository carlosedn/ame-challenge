package br.com.ame.challenge.amechallenge.service;

import br.com.ame.challenge.amechallenge.model.Planet;
import br.com.ame.challenge.amechallenge.repository.PlanetRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;

@Service
public class PlanetService {

    @Autowired
    private PlanetRepository planetRepository;
    @Autowired
    private StarWarsService starWarsService;

    public Planet savePlanet(Planet planet) throws URISyntaxException, JsonProcessingException {

        planet.setQtFilms(starWarsService.checkQuantityMovies(planet.getName()));

        return planetRepository.save(planet);
    }

}
