package br.com.ame.challenge.amechallenge.controller;

import br.com.ame.challenge.amechallenge.model.Planet;
import br.com.ame.challenge.amechallenge.model.ResponsePlanet;
import br.com.ame.challenge.amechallenge.service.PlanetService;
import br.com.ame.challenge.amechallenge.service.StarWarsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.Optional;

@Controller
public class PlanetController {

    @Autowired
    private StarWarsService starWarsService;
    @Autowired
    private PlanetService planetService;

    @GetMapping("/planetsFromStarWars")
    @ResponseBody
    public ResponsePlanet getPlanetsFromStarWars(@RequestParam("page") Optional<String> page) throws URISyntaxException, JsonProcessingException {
        return starWarsService.fetchPlanets(page.isPresent() ? page.get() : "1");
    }

    @PostMapping("/planets/save")
    @ResponseBody
    public Planet save(@RequestBody Planet planet) throws URISyntaxException, JsonProcessingException {
        return planetService.savePlanet(planet);
    }


}
