package br.com.ame.challenge.amechallenge.service;

import br.com.ame.challenge.amechallenge.common.RestClient;
import br.com.ame.challenge.amechallenge.model.Planet;
import br.com.ame.challenge.amechallenge.model.ResponsePlanet;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@Service
public class StarWarsService extends RestClient {

    @Value("${starWars.api.baseUrl}")
    private String baseURL;

    public ResponsePlanet fetchPlanets(String page) throws URISyntaxException, JsonProcessingException, HttpClientErrorException {

        ResponsePlanet res = new ResponsePlanet();

        ResponseEntity<String> result = this.getRest(baseURL+"planets/?page="+page);

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode aNod = objectMapper.readTree(result.getBody());

        JsonNode results = aNod.at("/results");
        Planet[] aPlanets = objectMapper.convertValue(results, new TypeReference<Planet[]>() {});

        JsonNode count = aNod.at("/count");
        res.setCount(Integer.valueOf(count.asText()));

        JsonNode next = aNod.at("/next");
        if (next != null && !next.asText().equals("null")) {
            MultiValueMap<String, String> parameters =
                    UriComponentsBuilder.fromUriString(next.asText()).build().getQueryParams();
            List<String> pageNr = parameters.get("page");
            res.setNext(ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString()+"?page="+pageNr.get(0));
        }
        JsonNode previous = aNod.at("/previous");
        if (previous != null && !previous.asText().equals("null")) {
            MultiValueMap<String, String> parameters =
                    UriComponentsBuilder.fromUriString(previous.asText()).build().getQueryParams();
            List<String> pageNr = parameters.get("page");
            res.setPrevious(ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString()+"?page="+pageNr.get(0));
        }

        res.setResults(Arrays.asList(aPlanets));

        return res;

    }

    public Integer checkQuantityMovies(String planetName) throws URISyntaxException, JsonProcessingException, HttpClientErrorException {

        Boolean retry = true;
        String url = baseURL + "planets/";
        do {
            ResponsePlanet res = new ResponsePlanet();

            ResponseEntity<String> result = this.getRest(url);

            ObjectMapper objectMapper = new ObjectMapper();

            JsonNode aNod = objectMapper.readTree(result.getBody());

            JsonNode next = aNod.at("/next");
            if (next != null && !next.asText().equals("null")) {
                url = next.asText();
            } else {
                retry = false;
            }

            JsonNode results = aNod.at("/results");
            PlanetAndMovies[] aPlanets = objectMapper.convertValue(results, new TypeReference<PlanetAndMovies[]>() {
            });
            for (PlanetAndMovies planet : Arrays.asList(aPlanets)) {
                if (planet.getName().equals(planetName)) {
                        return planet.getFilms().size();
                }
            }
        } while(retry);

        return 0;

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Getter
    static class PlanetAndMovies {
        String name;
        List<String> films;
    }

}
