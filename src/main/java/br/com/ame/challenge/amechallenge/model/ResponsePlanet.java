package br.com.ame.challenge.amechallenge.model;

import lombok.Data;

import java.util.List;

@Data
public class ResponsePlanet {

    private Integer count;
    private String next;
    private String previous;
    private List<Planet> results;

}
